import React from 'react'
import { Link } from 'react-router-dom'
import { useFetchPokemons, useFetchPokemonDetails } from '../../api/apiHooks'
import Loader from '../Loader/Loader'

import './index.scss'

type Pokemon = {
  name: string
  url: string
}

const PAGE_SIZE = 20

const PokemonList = () => {
  const [searchPokemon, setSearchPokemon] = React.useState('')
  const [page, setPage] = React.useState(0)
  const data = useFetchPokemons({ offset: page * PAGE_SIZE, pageSize: PAGE_SIZE })
  const { isLoading, isError, refetch } = useFetchPokemonDetails(
    searchPokemon.toLowerCase(),
    false
  )

  const isThereAPreviousPage = data.data?.previous
  const isThereANextPage = data.data?.next
  const pokemons = data.data?.results

  return pokemons ? (
    <div className='PokemonList'>
      <div className='PokemonList__Search'>
        <span>Search for a pokemon !</span>
        <input
          onChange={(e) => {
            setSearchPokemon(e.currentTarget.value)
          }}
          onKeyDown={(e) => {
            if (e.code === 'Enter') {
              refetch({ cancelRefetch: true })
            }
          }}
          placeholder={'Pokemon Name'}
          type='text'
          value={searchPokemon}
        />
        {isLoading && <Loader />}
        {isError && (
          <div className='PokemonList__Error'>
            This pokemon can not be found.
          </div>
        )}
      </div>
      {pokemons.map((pokemon: Pokemon) => (
        <Link
          key={pokemon.name}
          to={`/${pokemon.name}`}
          className='PokemonList__item'
        >
          {pokemon.name}
        </Link>
      ))}
      <div className='PokemonList__pagination'>
        <div
          className={`PokemonList__previousPage ${
            isThereAPreviousPage ? 'clickable' : ''
          }`}
          onClick={() => {
            if (isThereAPreviousPage) setPage(page - 1)
          }}
        >
          {'<'}
        </div>
        <div
          className={`PokemonList__nextPage ${
            isThereANextPage ? 'clickable' : ''
          }`}
          onClick={() => {
            if (isThereANextPage) setPage(page + 1)
          }}
        >
          {'>'}
        </div>
      </div>
    </div>
  ) : (
    <Loader />
  )
}

export default PokemonList
