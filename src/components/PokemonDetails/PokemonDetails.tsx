import React from 'react'
import { useParams } from 'react-router'
import { useFetchPokemonDetails } from '../../api/apiHooks'
import Loader from '../Loader/Loader'

import './index.scss'

type ObjectWithNameAndURL = {
  name: string
  url: string
}

type Stat = {
  base_stat: number
  stat: ObjectWithNameAndURL
}

type Ability = {
  ability: ObjectWithNameAndURL,
  is_hidden: boolean,
}

const PokemonDetails = () => {
  const { pokemonName } = useParams()

  const { data, isError, isLoading } = useFetchPokemonDetails(pokemonName, true)

  if (isLoading) return <Loader />

  if (isError) return <div>Pokemon not found.</div>

  const {
    abilities,
    id,
    name,
    sprites,
    stats,
    types,
  } = data

  return (
    <div className='PokemonDetails'>
      <div className='PokemonDetails__Title'>
        {name} <span>#{id}</span>
      </div>
      <div className='PokemonDetails__Grid'>
        <div className='PokemonDetails__Sprite'>
          <img src={sprites.front_default} alt='sprite' />
        </div>
        <div className='PokemonDetails__Infos'>
          <div className='PokemonDetails__Types'>
            <div className='PokemonDetails__Types--title'>Types</div>
            <div>
              {types.map(({ type }: { type: ObjectWithNameAndURL }) => (
                <span key={`${name}-type-${type.name}`}>{type.name} </span>
              ))}
            </div>
          </div>

          <div className="PokemonDetails__StatsTitle">Stats</div>
          <div className='PokemonDetails__Stats'>
            {stats.map((stat: Stat) => (
              <div key={`${name}-stat-${stat.stat.name}`} className='PokemonDetails__Stat'>
                <div className='PokemonDetails__Stats--title'>
                  {stat.stat.name}
                </div>
                <span>{stat.base_stat}</span>
              </div>
            ))}
          </div>
          
          <div className="PokemonDetails__Abilities">
            <div className="PokemonDetails__Abilities--title">Abilities</div>
            {abilities?.length > 0 ?
            <div className="PokemonDetails__AbilitiesContainer">
              {abilities.map((ability: Ability) => (
                <div key={`${name}-ability-${ability.ability.name}`} className="PokemonDetails__Ability">
                  {ability.ability.name}
                </div>
              ))}
            </div>
            : <div>This pokemon doesn't have abilities.</div>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default PokemonDetails
