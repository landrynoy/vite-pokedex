import React from 'react'
import { Link } from 'react-router-dom'

import './index.scss'

const Header = () => (
  <div className="Header">
    <Link to={`/`} className="Header__Home">
      Homemade Pokedex
    </Link>
  </div>
)

export default Header