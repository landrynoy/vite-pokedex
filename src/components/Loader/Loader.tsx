import React from 'react'

import './index.scss'

const Loader = () => (
  <div className="Loader"></div>
)

export default Loader