import { useQuery } from 'react-query'
import { useNavigate } from 'react-router'
import { fetchPokemons, fetchPokemonDetails } from './api'

interface FetchPokemonsParameters {
  offset: number
  pageSize: number
}

const useFetchPokemons = ({ offset, pageSize }: FetchPokemonsParameters) => {
  return useQuery(['useFetchPokemons', { offset, pageSize }], fetchPokemons, {
    refetchOnWindowFocus: false,
    staleTime: Infinity,
  })
}

const useFetchPokemonDetails = (
  pokemonName: string | undefined,
  enabled: boolean
) => {
  const navigate = useNavigate()

  return useQuery(
    ['useFetchPokemonDetails', pokemonName],
    fetchPokemonDetails,
    {
      cacheTime: 1000,
      enabled,
      retry: false,
      refetchOnWindowFocus: false,
      staleTime: Infinity,
      onSuccess: () => {
        if (!enabled) navigate(`/${pokemonName}`)
      },
    }
  )
}

export { useFetchPokemons, useFetchPokemonDetails }
