import axios from 'axios'

interface FetchPokemonParameters {
  queryKey: Array<any>
}

const fetchPokemons = (parameters: FetchPokemonParameters): Promise<any> => {
  const [, { offset, pageSize }] = parameters.queryKey

  return axios
    .get(`/api/v2/pokemon/?limit=${pageSize}&offset=${offset}`)
    .then((resp) => {
      return resp.data
    })
    .catch((err) => Promise.reject(err))
}

const fetchPokemonDetails = (
  parameters: FetchPokemonParameters
): Promise<any> => {
  const [, pokemonName] = parameters.queryKey

  return axios
    .get(`/api/v2/pokemon/${pokemonName}`)
    .then((resp) => {
      return resp.data
    })
    .catch((err) => Promise.reject(err))
}

export { fetchPokemons, fetchPokemonDetails }
