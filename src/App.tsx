import { Route, Routes } from 'react-router-dom'
import PokemonList from './components/PokemonList/PokemonList'
import PokemonDetails from './components/PokemonDetails/PokemonDetails'
import Header from './components/Header/Header'

import './App.css'

function App() {
  return (
    <div className='App'>
      <Header />
      <Routes>
        <Route path='/' element={<PokemonList />} />
        <Route path="/:pokemonName" element={<PokemonDetails />} />
        <Route path='/notfound' element={<div>NOT FOUND</div>} />
      </Routes>
    </div>
  )
}

export default App
