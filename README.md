A small application where you can see the list of pokemons with the pokeapi and search for one specifically and see some details of it.
Design is very minimalist, this is for an exercise

Please run 

npm install

to install

and npm run dev 

to run the app
